// ==UserScript==
// @name         Barclays Keep Me Logged In
// @namespace    http://www.bitbucket.org/danabrey
// @version      0.1.3
// @description  Keep me logged in to Barclays, don't log me out, it's so annoying
// @author       Dan Abrey
// @match        https://bank.barclays.co.uk/olb/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    var keepMeLoggedInLink = document.getElementById('session-expiry-extend');
    function clickLink() {
        keepMeLoggedInLink.click();
    }
    function linkIsHidden() {
        return (keepMeLoggedInLink.offsetParent === null);
    }
    function checkIfLinkVisible() {
        if (!linkIsHidden()) {
            clickLink();
        }
    }
    if (keepMeLoggedInLink !== null) {
        setInterval(checkIfLinkVisible, 1000);
    }
})();
