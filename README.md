# Barclays Online Banking: Stay Logged In #

This [Tampermonkey](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo) user script dismisses the "We're logging you out due to inactivity" modal on the Barclays Online Banking website.

## Installation ##

Once you have the Tampermonkey Chrome extension installed, install the user script by accessing the raw barclays-online-banking-stay-logged-in.user.js file [here](https://bitbucket.org/danabrey/barclays-online-banking-stay-logged-in/raw/master/barclays-online-banking-stay-logged-in.user.js). Tampermonkey will check the master branch of this repository periodically for updates.

## Disclaimer ##

Use at your own risk, obv.